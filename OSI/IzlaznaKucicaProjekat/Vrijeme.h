#pragma once
#include <ctime>
#include <iostream>
#include<string>
class Vrijeme
{
public:
	int god, mj, dan, hh, mm, ss;
	Vrijeme();
	Vrijeme(int gg, int mm, int dd, int hh, int min, int ss);
	Vrijeme(Vrijeme&);
	std::string prekoracenje(Vrijeme&, int);
	void operator=(Vrijeme &);
	friend std::ostream& operator<<(std::ostream &os, const Vrijeme&);
	int operator>(const Vrijeme&);
	void copy( Vrijeme &);
	int poredi(const Vrijeme&, const Vrijeme&);

};
