#pragma once
#include <stdio.h>
#include <iomanip>
#include <iostream>
#include "Vozilo.h"
#include <string>
#include "Vrijeme.h"
#include "CentriranjeTeksta.h"
using namespace std;

void pisiHeather(ostream& os);
void dodajuIzvjestaj(const Vozilo& vozilo, int izlaz, Vrijeme& vrijeme, string prekoracenje,string imeRadnika,string enp );
void kreirajIzvjestaj();

fstream pomocni;

void kreirajIzvjestaj()
{
	
	ifstream test;
	test.open("izvjestaj.txt");
	if (!test)
	{
		ofstream izlazni;
		izlazni.open("izvjestaj.txt", std::ios_base::app);
		pisiHeather(izlazni);
	}

}

void dodajuIzvjestaj(const Vozilo& vozilo, int izlaz, Vrijeme& vrijeme, string prekoracenje, string imeRadnika,string enp)
{
	ofstream os;
	os.open("izvjestaj.txt",ios::app);
	
	os << setw(12) << centered(vozilo.registraija) << "|";
	os << setw(8) << centered(to_string(vozilo.ulaz))<<"|";
	os << setw(9) << centered(to_string(izlaz)) << "|";
	os << setw(10) << centered(to_string(vozilo.kategorija)) << "|";
	os << vozilo.ulazak << "|";
	os << vrijeme << "|";
	os << setw(12) << centered(prekoracenje) << "|";
	os << setw(3) << centered(enp) << "|";
	os << setw(20) << centered(imeRadnika) <<"|"<< endl<<endl;
	os.close();

}
void pisiHeather(ostream& os)
{
	os << "************************************************************************************************************************" << endl;
	os << "Registracija|br.ulaza|br.izlaza|kategorija|   vrijeme ulaza   |   vrijeme izlaza  |prekoracenje|ENP|       radnik       |" << endl;
	os << "************************************************************************************************************************" << endl;
}	


