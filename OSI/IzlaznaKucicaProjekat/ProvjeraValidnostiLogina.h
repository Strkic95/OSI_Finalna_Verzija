#pragma once
#include <iostream>
#include <fstream>
#include <string>
std::string trenutniKorisnik; 

bool ProvjeraValidnostiLogina(const std::string &ime, const std::string &pass) {
	std::ifstream file("log.txt");
	std::string Ko_Ime, Ko_Pass;

	while (file) {
		std::getline(file, Ko_Ime, ';'); // use ; as delimiter
		std::getline(file, Ko_Pass); // use line end as delimiter
									   // remember - delimiter readed from input but not added to output
		if (Ko_Ime == ime && Ko_Pass == pass)
		{
			trenutniKorisnik = Ko_Ime;
			return true;
		}
	}

	return false;
}