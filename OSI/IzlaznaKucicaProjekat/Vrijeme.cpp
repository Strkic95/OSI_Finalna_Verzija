#define _CRT_SECURE_NO_WARNINGS
#include "Vrijeme.h"

Vrijeme::Vrijeme()
{
	time_t theTime = time(NULL);
	struct tm *aTime = localtime(&theTime);
	dan = aTime->tm_mday;
	mj = aTime->tm_mon + 1; // Month is 0 - 11, add 1 to get a jan-dec 1-12 concept
	god = aTime->tm_year + 1900; // Year is # years since 1900
	hh = aTime->tm_hour;
	mm = aTime->tm_min;
	ss = aTime->tm_sec;
}

Vrijeme::Vrijeme(int ggod, int mmj, int ddan, int hhh, int mmm, int sss)
{
	god = ggod;
	mj = mmj;
	dan = ddan;
	hh = hhh;
	mm = mmm;
	ss = sss;
}

Vrijeme::Vrijeme(Vrijeme &pom)
{
	copy(pom);
}

std::string Vrijeme::prekoracenje(Vrijeme &ulaz, int min)//prima izlazno vrijeme
{
	int duzinaPutovanja=0;
	if (mj > ulaz.mj)  duzinaPutovanja +=   24 * 60;
	else if (ulaz.mj > dan) duzinaPutovanja +=  24 * 60;//samo preklopljen jedan dan kad ides na novi mj(vozis se nocu pa udjes u novi mj)
	if (dan > ulaz.dan)  duzinaPutovanja += (dan - ulaz.dan) * 24 * 60;
	else if (ulaz.dan > dan) duzinaPutovanja += (ulaz.dan - dan) * 24 * 60;
	if (hh > ulaz.hh)  duzinaPutovanja += (hh - ulaz.hh)  * 60;
	else if (ulaz.hh > hh) duzinaPutovanja += (ulaz.hh - hh)  * 60;
	if (mm > ulaz.mm)  duzinaPutovanja += (mm - ulaz.mm);
	else if (ulaz.mm > mm) duzinaPutovanja += (ulaz.mm - mm); 

	if (duzinaPutovanja < min) return "da";
	else return "ne";
}

void Vrijeme::operator=(Vrijeme &pom)
{
	copy(pom);
}

int Vrijeme::operator>(const Vrijeme &pomocno)//ako je prvo vece od drugog vraca 1, ako je drugo vece od prvo vraca 2 u suprotnom vraca 0
{
	int veci = poredi(*this, pomocno);
	if (veci == 1) return 1;
	else veci = poredi(pomocno, *this);
	if (veci == 1) return 2;
	return 0;
}

void Vrijeme::copy(Vrijeme &pom2)
{
	god = pom2.god;
	mj = pom2.mj;
	dan = pom2.dan;
	hh = pom2.hh;
	mm = pom2.mm;
	ss = pom2.ss;
}

int Vrijeme::poredi(const Vrijeme &vrijeme1, const Vrijeme &vrijeme2)
{
	if (vrijeme1.god > vrijeme2.god) return 1;
	else if (vrijeme1.mj > vrijeme2.mj) return 1;
	else if (vrijeme1.dan > vrijeme2.dan) return 1;
	else if (vrijeme1.hh > vrijeme2.hh) return 1;
	else if (vrijeme1.mm > vrijeme2.mm) return 1;
	else if (vrijeme1.ss > vrijeme2.ss) return 1;
	else return 0;
}

std::ostream & operator<<(std::ostream & os, const Vrijeme &pom)
{
	if (pom.dan < 10) os << "0" << pom.dan << "."; else os << pom.dan << ".";
	if (pom.mj < 10) os << "0" << pom.mj << ".";	else os << pom.mj << ".";
	os << pom.god << " ";
	if (pom.hh < 10) os << "0" << pom.hh << "."; else os << pom.hh << ".";
	if (pom.mm < 10) os << "0" << pom.mm << "."; else os << pom.mm << ".";
	if (pom.ss < 10) os << "0" << pom.ss; else os << pom.ss;
	//os <<pom.dan << "." << pom.mj << "." << pom.god << " " << pom.hh << ":" << pom.mm << ":" << pom.ss;
	return os;
}
