#pragma once
#include <iostream>
#include "Vrijeme.h"
#include <string>

class Vozilo
{
public:
	Vrijeme ulazak;
	int ulaz;
	int kategorija;
	std::string registraija;
	Vozilo(Vrijeme&,int,int,std::string);
	void operator=(Vozilo&);
	Vozilo();
};
