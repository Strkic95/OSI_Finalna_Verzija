#pragma once
#include<iostream>
#include<string>

#include "Vrijeme.h"
using namespace std;
void podesiVrijeme(Vrijeme& pom, string dat, string vr) //koristi se u pronadjiVozilo, a gdje trazi vozilo u datoteci radi naplate putarine
{
	pom.dan = (dat[0] - 48) * 10 + (dat[1] - 48);
	pom.mj = (dat[3] - 48) * 10 + (dat[4] - 48);
	pom.god = (dat[6] - 48) * 1000 + (dat[7] - 48) * 100 + (dat[8] - 48) * 10 + (dat[9] - 48);
	pom.hh = (vr[0] - 48) * 10 + (vr[1] - 48);
	pom.mm = (vr[3] - 48) * 10 + (vr[4] - 48);
	pom.ss = (vr[6] - 48) * 10 + (vr[7] - 48);
}