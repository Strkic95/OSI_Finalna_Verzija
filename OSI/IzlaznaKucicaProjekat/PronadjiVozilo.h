#pragma once
#include<string>
#include"Vozilo.h"
#include <fstream>
#include"PodesiVrijeme.h"
using namespace std;
int pronadjiVozilo(std::string registracija, fstream& file, Vozilo& car)//prepravljeno da trazi poslednji ulazak u nizu ulazaka
{
	string reg, dat, vr, pom;
	int kat, ulaz1, pronasao = 0;
	Vrijeme pomocno;
	Vozilo pomocnoVozilo;

	while (getline(file, reg, ','))
	{
		if (!registracija.compare(reg))
		{

			getline(file, pom, ',');
			kat = stoi(pom);
			getline(file, pom, ',');
			ulaz1 = stoi(pom);
			getline(file, dat, ',');
			getline(file, vr, ',');
			podesiVrijeme(pomocno, dat, vr);
			car.ulazak = pomocno;
			car.registraija = registracija;
			car.kategorija = kat;
			car.ulaz = ulaz1;
			if (!pronasao)//prvo vozilo cuvam kao najvece pa kasije poredim sa ostalima
			{
				pomocnoVozilo = car;
				pronasao = 1;
			}
			if ((car.ulazak>pomocnoVozilo.ulazak)==1)//to kasnije je stiglo, sad poredim i trazim poslednji ulazak
			{
				pomocnoVozilo = car;
			}
		}


	}
	car = pomocnoVozilo;
	return pronasao;
}