#pragma once
#include<iostream>
#include<exception>

class Error :std::exception
{
public :
	virtual const char * what() const override;
};